//自定全局变量
var a = 0
//内部全局变量
//舞台数据
var Gl_stage = null
//文本框数据
var Gl_text = null
//全局音频句柄
var audios = new Array()
//全局过程队列（执行函数名）
var queue = new Array()
//设定全局变量方法
function setGlobalVar(GlobalVar) {
    a = GlobalVar
}
//初始化舞台
function setStage(id, parent, width, height, top = 0, left = 0) {
    if (Gl_stage != null) {
        console.log("舞台只允许初始化一次")
        return false
    } else {
        Gl_stage = id
        //Gl_stage['width'] = width
        //Gl_stage['height'] = height
        $(parent).append("<div id = '" + id + "'></div>")
        $("#" + id).css({
            "position": "absolute",
            "width": width + "px",
            "padding": "0px",
            "height": height + "px",
            "background-size": "100% 100%",
            "margin": "0px",
        })
        return true
    }
}
//设定舞台背景(并绘制)
function setStageBackGround(src) {
    $("#" + Gl_stage).css("background-image", "url(" + src + ")")
    return true
}
//设定文本框(并绘制)
function setStageText(width, height, top, left, color) {
    if (Gl_text != null) {
        console.log("文本框只允许出现一次")
        return false
    } else {
        if (Gl_stage == null) {
            console.log("舞台未被初始化")
            return false
        } else {
            Gl_text = Gl_stage + "_text"
            $("#" + Gl_stage).append("<div id = '" + Gl_text + "'></div>")
            $("#" + Gl_text).css({
                "position": "absolute",
                "margin": "0px",
                "width": width - 20,
                "height": height - 20,
                "left": left,
                "top": top,
                "background-color": color,
                "padding": "10px",
                "font-size": "20px",
                "color": "white"
            })
        }
    }
}
//修改文本框内容
function setText(data) {
    if (Gl_stage == null) {
        console.log("舞台未被初始化")
        return false
    } else {
        $("#" + Gl_text).text(data)
    }
}
//添加图片（先来后到懂么。先来者居上）
function appendImg(src, id, width, height, top, left) {
    $("#" + Gl_stage).prepend("<div id ='" + id + "'></div>")
    $("#" + id).css({
        "position": "absolute",
        "margin": "0px",
        "width": width,
        "height": height,
        "left": left,
        "top": top,
        "background-image": "url(" + src + ")",
        "background-size": "100% 100%",
        "padding": "0px"
    })
}
//删除ID对应的元素（终极镇用）
function delelem(id) {
    $("#" + id).remove()
}
//添加音频元素（不播放）(不共用全局ID请使用专供函数)
function addBGM(src, id, loops = false) {
    audios[id] = new Audio(src)
    audios[id].preload = "auto";
    audios[id].loop = loops
}
//播放音频（重新播放）
function playBGM(id) {
    audios[id].currentTime = 0
    audios[id].play()
}
//停止播放
function stopBGM(id) {
    audios[id].pause()
}
//执行一个函数
function exeFun(fun, parameter = "") {
    eval(fun + "(" + parameter + ")")
}
//放置一个选项卡
function addSelect(data, funs, id, width, height, top, left, backcolor, hovercolor) {
    $("#" + Gl_stage).append("<div id = '" + id + "'>" + data + "</div>")
    $("#" + id).css({
        "position": "absolute",
        "width": width,
        "height": height,
        "padding": 0,
        "margin": 0,
        "top": top,
        "left": left,
        "text-align": "center",
        "line-height": height + "px",
        "color": "white",
        "background": backcolor,
        "font-size": "20px"
    })
    $("#" + id).hover(function () {
        $("#" + id).css("background", hovercolor)
    }, function () {
        $("#" + id).css("background", backcolor)
    })
    $("#" + id).click(function () {
        exeFun(funs[0],funs[1])
    })
}